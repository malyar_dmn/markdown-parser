import { Component, OnDestroy, OnInit } from '@angular/core';
import { Editor } from 'ngx-editor';

@Component({
  selector: 'app-text-editor',
  templateUrl: './text-editor.component.html',
  styleUrls: ['./text-editor.component.scss'],
})
export class TextEditorComponent implements OnInit, OnInit, OnDestroy {
  editor: Editor;
  text: string;
  constructor() {}

  ngOnInit(): void {
    this.editor = new Editor();
    this.editor.valueChanges.subscribe((data) =>
      console.log(data['content'][0].content)
    );
  }

  ngOnDestroy(): void {
    this.editor.destroy();
  }
}
