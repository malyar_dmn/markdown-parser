import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { MaterialModule } from 'src/app/material/material/material.module';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { NgxEditorModule } from 'ngx-editor';
import { FormsModule } from '@angular/forms';
import { UserListComponent } from './components/user-list/user-list.component';
import { TextEditorComponent } from './components/text-editor/text-editor.component';

@NgModule({
  declarations: [HomePageComponent, ToolbarComponent, UserListComponent, TextEditorComponent],
  imports: [CommonModule, MaterialModule, NgxEditorModule, FormsModule],
})
export class HomeModule {}
